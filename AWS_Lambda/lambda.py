import os
import boto3
import json

print('Loading function')
sns = boto3.client('sns')
ses = boto3.client('ses')
ses_source = os.environ['SOURCE_EMAIL']


def lambda_handler(event, context):
    '''
    This handler is activated from an API call for the Amazon Watchlist Chrome
    extension. The event contains a json serialize string body with this format:
    {
        'appName': 'Watchlist',
        'email': 'user's email',
        'phoneNumber': 'user's phone number'
        'alerts': ['str1', 'str2',...]
    }
    
    This handler will contact the user using their email and/or phone number
    about the following alerts messages.
    '''
    
    # For CloudWatch Logs
    print("Received event: " + json.dumps(event, indent=2))
    operation = event['httpMethod']
    
    # Preflight check
    if operation == 'OPTIONS':
        return respond(None, {})
    
    if operation != 'POST':
        return respond({'message': operation + ' method is not supported.'})
        
    if event['body'] is None or len(event['body']) == 0:
        return respond({'message': 'Empty body'})
        
    payload = json.loads(event['body'])
    
    if payload['appName'] != 'Watchlist':
        return respond({'message': 'Invalid App'})
    
    ## Comment 2 lines below to enable notification
    if True:
        return respond(None, {'message': 'Notification is currently disable'})
        
    result = alert_user(payload)
    
    return respond(None, result)


def respond(err, res=None):
    return {
        'statusCode': '400' if err else '200',
        'body': json.dumps(err) if err else json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type'
        },
    }
    

def alert_user(payload):
    res = {}
    res['emailSent'] = send_email(payload)
    res['textSent'] = send_text_message(payload)
    
    return res


def send_email(payload):
    '''
    Sends an email to user.
    '''
    
    if 'email' not in payload:
        return None
    
    to_address = payload['email']
    alerts = payload['alerts']
    msg = 'The following {} item(s) are now under your price range at Amazon:' \
            .format(len(alerts))
    msg += '\n\n'
    
    for alert in alerts:
        msg += alert
        msg += '\n\n'
    
    message = {}
    message['Subject'] = {'Data': 'Amazon Watchlist Alert', 'Charset': 'utf-8'}
    message['Body'] = {'Text': {'Data': msg, 'Charset' : 'utf-8'}}
    destination = {'ToAddresses': [to_address]}
    
    ses.send_email(Source=ses_source, Destination=destination, Message=message)
    
    return "Email sent"


def send_text_message(payload):
    '''
    Send text message to user. Only sends the total number of items to the user.
    '''
    
    if 'phoneNumber' not in payload or len(payload['phoneNumber']) <= 1:
        return None
    
    user_phone_number = payload['phoneNumber']
    alerts = payload['alerts']
    sns_message = "You have {} item(s) at or under your desired pricing." \
                    .format(len(alerts))
                    
    sns.publish(PhoneNumber=user_phone_number, Message=sns_message)
    
    return "Text message sent"

