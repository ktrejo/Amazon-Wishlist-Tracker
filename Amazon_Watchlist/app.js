var app = angular.module("watchlistApp", []);


app.controller("setupController", ["$scope", "$http", "$log", function($scope, $http, $log) {

    $scope.wishlistID = null;
    
    $scope.setListID = function(id) {

        $scope.wishlistID = id;
        // set wishlist id and empty JSON object
        chrome.storage.local.set({"wishlistID": id, "items" : JSON.stringify({})}, function() {
            chrome.tabs.getCurrent(function(tab) {
                chrome.tabs.remove(tab.id, function() {});
            });
        });

    }

}]);



app.controller("optionsController", ["$scope", "$log", function($scope, $log) {

    $scope.currentID = "";
    $scope.email = "";
    $scope.phoneNumber = "";
    $scope.currentView = 0;

    $scope.getInfo = function() {

        chrome.storage.local.get(["wishlistID", "userInfo"], function(result) {
            $log.log(result);

            if (result.wishlistID == undefined)
                $scope.currentID = "None";
            else
                $scope.currentID = result.wishlistID;

            if (result.userInfo != undefined) {
                var userInfo = JSON.parse(result.userInfo);
                $scope.email = userInfo.email;
                $scope.phoneNumber = userInfo.phoneNumber;
            }

            // Update change to the view bound to currentID
            $scope.$digest();

        });
    }

    $scope.changeView = function(id) {
        $scope.currentView = id;
    }
    
    $scope.setListID = function(id) {

        // set wishlist id and empty JSON object
        chrome.storage.local.set({"wishlistID": id, "items" : JSON.stringify({})}, function() {
            $scope.currentID = id;
            $scope.$digest();
        });

    }

    $scope.setUserInfo = function() {
        $log.log("updating user info");
        var userInfo = {"email": $scope.email, "phoneNumber": $scope.phoneNumber};
        chrome.storage.local.set({"userInfo" : JSON.stringify(userInfo)}, null);
    }

    $scope.getInfo();

}]);



app.controller("backgroundController", ["$log", "$interval", "wishlistFactory", function($log, $interval, wishlistFactory) {

    function checkAmazonPricing() {
        wishlistFactory.acquireProducts();
    }

    // Interval in milliseconds. 1000 * 60 * 60 * 6 = 6 hours
    $interval(checkAmazonPricing, 1000 * 60 * 60 * 6);

    checkAmazonPricing();

}]);


app.controller("popupController", ["$scope", "$timeout", "$log", "wishlistFactory", function($scope, $timeout, $log, wishlistFactory) {

    $scope.wishlistID = null;
    $scope.productList = [];
    $scope.finishLoading = false;

    $scope.comparePrice = function(product) {
        if (isNaN(product["alertPrice"])) {
            return false;
        }

        return product["price"] <= product["alertPrice"];
    }

    $scope.modifyAlertPrice = function(asin) {
        $log.log("Modifying Alert Price");

        // change to display set new price
        if ($("#"+asin).has("p").length == 1) {
            var currentAlertPrice = $scope.productList[wishlistFactory.asinMap[asin]].alertPrice;
            $("#"+asin).empty().append("<input type='text' class='form-control form-control-sm' placeholder='"+currentAlertPrice+"'/>");
            $("#button-"+asin).text("Set Price");

        } else {
            $log.log("updating alert price for " + asin);

            var pos = wishlistFactory.asinMap[asin];
            var newAlertPrice = $("#"+asin+" > input").val();

            // update and store new price if enter value is a valid number
            if (!(newAlertPrice == "" || isNaN(newAlertPrice))) {

                newAlertPrice = String(Number(newAlertPrice).toFixed(2));

                // Update data model
                let currProduct = $scope.productList[pos];
                currProduct.alertPrice = newAlertPrice;
                // Disable this if you don't want email/text notification 
                // to occur anytime user interacts with popup view 
                // currProduct.alertedUser = false;

                // Update local storage copy
                currProduct = wishlistFactory.currentProductsToStore[asin];
                currProduct.alertPrice = newAlertPrice;
                currProduct.alertedUser = false;
                chrome.storage.local.set({"items" : JSON.stringify(wishlistFactory.currentProductsToStore)}, function() {
                    wishlistFactory.compareCurrentPricesToAlertPrices();
                });

            }

            // change back to display alert price
            $("#"+asin).empty().append("<p>$"+$scope.productList[pos].alertPrice+"</p>");
            $("#button-"+asin).text("Change Price");
  
        }

    }

    $scope.toOptionsPage = function() {
        chrome.tabs.create({url: "./options.html"});
    }

    var checkAcquireProductsFinish = function() {
        if (wishlistFactory.wishlistID == null) {
            $timeout(checkAcquireProductsFinish, 1000);

        } else {
            $scope.wishlistID = wishlistFactory.wishlistID;
            $scope.productList = wishlistFactory.productList.slice();
            $scope.finishLoading = true;
        }
    }

    wishlistFactory.acquireProducts();
    $timeout(checkAcquireProductsFinish, 1000);

}]);



app.service("awsService", ["$http", "$log", function($http, $log) {
    this.lambdaUrl = "TODO";
    this.alertUser = function(userInfo, products) {
        $log.log(this.lambdaUrl);
        $log.log("ERROR, you need to setup aws lambda function");

        var payload = {};
        var alerts = [];

        payload.appName = "Watchlist";
        payload.email = userInfo.email;
        payload.phoneNumber = userInfo.phoneNumber;
        payload.alerts = alerts;
        products.forEach(element => {
            let msg = element.productTitle;
            msg += "\nAlert Price: " + element.alertPrice;
            msg += " Current Price: " + element.price;
            msg += "\nLink: " + element.webLink;
            alerts.push(msg);
        });
        
        payload = JSON.stringify(payload);
        //$log.log(payload);

        $http.post(this.lambdaUrl, payload).then(res => {
            $log.log("AWS Success")
            $log.log(res);
        }, err => {
            $log.log("AWS Error");
            $log.log(err);
        })

    };

}]);


app.factory("wishlistFactory", ["$http", "$log", "awsService", function($http, $log, awsService) {

    var amazonWishlistURL = "https://www.amazon.com/registry/wishlist/";
    var wishlistFactory = {};

    wishlistFactory.wishlistID = null;
    // Contains all products info listed in the user's wishlist. (Pulled From Amazon)
    wishlistFactory.productList = [];
    // Contains the mapping of asin to productList index
    wishlistFactory.asinMap = {};
    // Contains products info listed in the user's wishlist. (Locally stored)
    wishlistFactory.currentProductsToStore = {};
    wishlistFactory.amazonWishlistURL = amazonWishlistURL;

    wishlistFactory.getIdFromStorage = function() {
        chrome.storage.local.get(["wishlistID"], function(result) {
            if (result.wishlistID == undefined)
                wishlistFactory.wishlistID = "None";
            else
                wishlistFactory.wishlistID = result.wishlistID;
        });
    }

    /*
    * Acquires asin, title, url, img, and current amazon price.
    */
    wishlistFactory.acquireProducts = function() {

        chrome.storage.local.get(["wishlistID"], function(result) {
            // wishlist id has not been set or has been deleted
            if (result.wishlistID == undefined) {
                wishlistFactory.wishlistID = "None";
                $log.log("wishlist has not been set");

            } else {
                wishlistFactory.wishlistID = result.wishlistID;
                acquireProducts();
            }
        });

    }

    /*
    * Compare current Amazon prices to user's alert prices.
    * Alerts user when more than 1 Amazon product is at or below
    * the user's desired price
    */
    wishlistFactory.compareCurrentPricesToAlertPrices = function() {

        var lowerThanAlertCount = 0;
        var productsToAlertUser = [];

        if (wishlistFactory.currentProductsToStore === {})
            return;

        for (let i = 0; i < wishlistFactory.productList.length; i++) {

            let product = wishlistFactory.productList[i];

            // Skip if alert price is not set or curent price is unavailable
            if (product.alertPrice === "Not Set" || product.price === "-1")
                continue;

            if (Number(product.price) <= Number(product.alertPrice)) {
                lowerThanAlertCount++;
                if (product.alertedUser != true) {
                    product.alertedUser = true;
                    productsToAlertUser.push(product)
                }
            }

        }

        let notificationNumber = (lowerThanAlertCount == 0)? "" : String(lowerThanAlertCount);
        chrome.browserAction.setBadgeText({text: notificationNumber});
        chrome.browserAction.setBadgeBackgroundColor({color: [184, 30, 30, 255]});

        if (productsToAlertUser.length != 0) {
            // Get User info for contact
            chrome.storage.local.get(["userInfo"], function(result) {
                let res = (result.userInfo == undefined)? null : JSON.parse(result.userInfo);

                // User did not set their information yet.
                if (res == null)
                    return;

                // Update alertedUser status and save it to local storage.
                // This step MUST occur otherwise the app would alert the
                // user on the same product(s) everytime the function
                // compareCurrentPricesToAlertPrices is called.
                productsToAlertUser.forEach(element => {
                    wishlistFactory.currentProductsToStore[element.asin].alertedUser = true;
                });
                chrome.storage.local.set({"items" : JSON.stringify(wishlistFactory.currentProductsToStore)});

                awsService.alertUser(res, productsToAlertUser);
            });
        }

    }

    /*
    * stores the results in wishlistFactory.productList
    * each element in the list has the follow structure:
    * { 
    *   asin : (String) product's asin, 
    *   price : (String) product's price (only displays Amazon, NO third party)
    *   alertPrice: (String) price set by user.
    *   alertedUser: (Boolean) notification to user was sent 
    *   productTitle : (String) product's title
    *   webLink : (String) Amazon's url for product
    *   img : (String) product's image url   
    * }
    */
    function acquireProducts() {
        $http.get(amazonWishlistURL + wishlistFactory.wishlistID).then(function(amazonResponse) {
            var $amazon = $.parseHTML(amazonResponse.data);
            var tags = $($amazon).find('#item-page-wrapper').find('li[data-id='+wishlistFactory.wishlistID+']');
            var pageInfo = $($amazon).find('script[type=a-state]');
            
            pageInfo = $(pageInfo).filter(function(index, element) {
                let json = JSON.parse(element.dataset.aState);
                return json.key == 'pageInfo';
            });
            pageInfo = JSON.parse(pageInfo[0].innerHTML);

            //console.log(pageInfo);

            var viewTypeIsGrid = pageInfo.viewType === "grid";
            var result = acquireProductsHelper(tags, viewTypeIsGrid)

            wishlistFactory.productList = result.productList;
            wishlistFactory.asinMap = result.asinMap;

            retrieveProductsFromStorage(result.productList);

        });
    }

    /*
    * Helper method to acquireProducts.
    */
    function acquireProductsHelper(tags, viewTypeIsGrid) {

        var productList = [];
        var asinMap = {};
        var products = {};

        for (let i = 0; i < tags.length; i++) {

            let productEntry = processProductTag(tags[i], viewTypeIsGrid);

            if (productEntry.asin != undefined) {
                asinMap[productEntry.asin] = productList.length;
                productList.push(productEntry);        
            }

        }

        products.productList = productList;
        products.asinMap = asinMap;

        return products;

    }

    /*
    * extract product information from indiviual li tag passed from acqureProductsHelper.
    * Returns an object containing asin, productTitle, webLink, price, img
    */
    function processProductTag(liTag, viewTypeIsGrid) {

        var productEntry = {};
        var aTag = $(liTag).find('a[class=a-link-normal]');

        productEntry.asin = liTag.dataset.repositionActionParams.match(/ASIN:([a-z0-9]+)/i)[1];
        productEntry.productTitle = aTag[0].title;
        productEntry.webLink = "https://www.amazon.com" + aTag[0].pathname;

        if (viewTypeIsGrid) {
            var spanTag = $(liTag).find('span[class=a-declarative][data-action=grid-add-to-cart]');
            
            if (spanTag.length == 0) {
                return {};
            }

            productEntry.price = JSON.parse(spanTag[0].dataset.gridAddToCart).price;
            productEntry.img = aTag[0].parentNode.children[2].firstElementChild.src;
        } else {

            productEntry.price = (liTag.dataset.price.includes("Infinity")) ? "-1" : liTag.dataset.price;
            productEntry.img = aTag[0].firstChild.src;
        }

        return productEntry;

    }

    /*
    * Obtains product information and alert prices stored in local storage.
    */
    function retrieveProductsFromStorage(productList) {
        chrome.storage.local.get(["items"], function(result) {

            var productsFromStorage = (result.items == undefined)? {} : JSON.parse(result.items);
            var currentProductsToStore = {};

            for (let i = 0; i < productList.length; i++) {

                let product = productList[i];
                let productToStore = {};
                let alertPrice = (productsFromStorage[product.asin] == undefined)? "Not Set" : productsFromStorage[product.asin].alertPrice;
                let alertedUser = (productsFromStorage[product.asin] == undefined)? false : productsFromStorage[product.asin].alertedUser;

                product.alertPrice = alertPrice;
                product.alertedUser = alertedUser;

                productToStore.alertPrice = alertPrice;
                productToStore.alertedUser = alertedUser;
                currentProductsToStore[product.asin] = productToStore;

            }

            $log.log("retrieveProductsFromStorage");
            $log.log(currentProductsToStore);

            wishlistFactory.currentProductsToStore = currentProductsToStore;

            // store only current products to watch
            chrome.storage.local.set({"items" : JSON.stringify(wishlistFactory.currentProductsToStore)});

            wishlistFactory.compareCurrentPricesToAlertPrices();
        });
    }

    return wishlistFactory;

}]);