// Fires when extension is either first installed, extension updated, or chrome version updated  
chrome.runtime.onInstalled.addListener(function (details) {

    // open setup only when wishlistID is empty
    chrome.storage.local.get(["wishlistID"], function(result) {

        if (result.wishlistID == undefined) {
            chrome.tabs.create({url: "./setup.html"}, function(tab) {
                console.log("setup page open")
            });
        } else {
            console.log("wishlistID already set");
        }

    });
    

})