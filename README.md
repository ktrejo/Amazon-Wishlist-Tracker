# Amazon Wishlist Chrome Extension

A Chrome Extension that monitors a public Amazon List. Capable of notifying user of price drops through Chrome Extension notification, email, and/or text message. **NOTE:** Notifications through email and text messages requires a server with REST endpoint. This project uses AWS Lambda to achieve this.

### Prerequisites
* Chrome Desktop Browser
* AWS Lambda REST Endpoint **(Optional - For email and text message service)**
  * AWS SES
  * AWS SNS

### Installing
Chrome Extension
* Open Chrome Extensions menu and turn on developer mode
* Select Load unpacked button and select Amazon_Watchlist folder

**Optional** - AWS Lambda
* Create an AWS Lambda REST endpoint with permissions for AWS Cloudwatch, SES(Simple Email Service), and SNS(Simple Notification Service)
* Insert python script under AWS_Lambda folder.
